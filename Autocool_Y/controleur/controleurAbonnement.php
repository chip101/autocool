<?php



$unFormulaire = new Formulaire('post','index.php','abonnement','formAbonnement');

$labelCivilite = $unFormulaire->creerLabel('Civilite :');
$inputCivilite = $unFormulaire->creerInputTexte('Civilite', 'inpCivilite', '' ,'required','ex : Madame/Monsieur/Mademoiselle/Mondemoiseau' , '[a-zA-Z0-9]+');

$Civilite = $unFormulaire->concactComposants($labelCivilite, $inputCivilite);





$labelPrenom = $unFormulaire->creerLabel('*Prenom :');
$inputPrenom = $unFormulaire->creerInputTexte('Prenom', 'inpPrenom', '' ,'required','ex : Jean-Pierre' , '[a-zA-Z0-9]+');

$Prenom = $unFormulaire->concactComposants($labelPrenom, $inputPrenom);

$labelNom = $unFormulaire->creerLabel('*Nom :');
$inputNom = $unFormulaire->creerInputTexte('Nnom', 'Nom', '' ,'required','ex : Dupuy' , '[a-zA-Z0-9]+');

$Nom = $unFormulaire->concactComposants($labelNom, $inputNom);

$labelSexe = $unFormulaire->creerLabel('Sexe :');
$tabSexe = array("Monsieur", "Madame");
$SelectSexe = $unFormulaire->creerSelect('ListeDeroulanteSexe','SelectSexe','Sexe :',$tabSexe);

$Sexe = $unFormulaire->concactComposants($labelSexe, $SelectSexe);


$labelRue = $unFormulaire->creerLabel('Rue :');
$inputRue = $unFormulaire->creerInputTexte('Rue', 'Rue', '' ,'required','ex : 12 place de la Nation' , '[a-zA-Z0-9]+');

$Rue = $unFormulaire->concactComposants($labelRue, $inputRue);

$labelVille = $unFormulaire->creerLabel('Ville :');
$inputVille = $unFormulaire->creerInputTexte('Ville', 'Ville', '' ,'required','ex : Guinguamp' , '[a-zA-Z0-9]+');

$Ville = $unFormulaire->concactComposants($labelVille, $inputVille);

$labelCP = $unFormulaire->creerLabel('Code Postal :');
$inputCP = $unFormulaire->creerInputTexte('CP', 'CP', '' ,'required','ex : 22200' , '[0-9]+');

$CP = $unFormulaire->concactComposants($labelCP, $inputCP);

$labelVille = $unFormulaire->creerLabel('Ville :');
$inputVille = $unFormulaire->creerInputTexte('Ville', 'Ville', '' ,'required','ex : Guinguamp' , '[a-zA-Z0-9]+');

$Ville = $unFormulaire->concactComposants($labelVille, $inputVille);

$labelFixe = $unFormulaire->creerLabel('T&#233;l&#233;phone fixe :');
$inputFixe = $unFormulaire->creerInputTexte('Fixe', 'Fixe', '' ,'required','ex : 01 07 45 64 79' , '[0-9]+');

$Fixe = $unFormulaire->concactComposants($labelFixe, $inputFixe);

$labelMobile = $unFormulaire->creerLabel('T&#233;l&#233;phone Mobile :');
$inputMobile = $unFormulaire->creerInputTexte('Mobile', 'Mobile', '' ,'required','ex : 06 07 45 64 79' , '[0-9]+');

$Mobile = $unFormulaire->concactComposants($labelMobile, $inputMobile);

$labelJour = $unFormulaire->creerLabel('Date de naissance sous le format Jour Mois Ann&#233;e :');
$inputJour = $unFormulaire->creerInputTexte('Jour', 'Jour', '' ,'required','ex : 4 et non 04' , '[0-9]+');

$Jour = $unFormulaire->concactComposants($labelJour,$inputJour);

$inputMois = $unFormulaire->creerInputTexte('Mois', 'Mois', '' ,'required','ex : 3 et non 03' , '[0-9]+');

$inputAnnee = $unFormulaire->creerInputTexte('Annee', 'Annee', '' ,'required','ex : 1995 et non 95' , '[0-9]+');

$labelPermisH2 = $unFormulaire->creerLabelH2('Information sur votre permis :');

$labelNP = $unFormulaire->creerLabel('Num&#233;ro de permis :');
$inputNP = $unFormulaire->creerInputTexte('NumPermis', 'NumPermis', '' ,'required',' 12 chiffres ' , '[0-9]+');

$NP = $unFormulaire->concactComposants($labelNP, $inputNP);


$labelLieuPermis = $unFormulaire->creerLabel('Lieu du permis :');
$inputLieuPermis= $unFormulaire->creerInputTexte('LieuPermis', 'LieuPermis', '' ,'required','ex : Carhaix' , '[a-zA-Z0-9]+');

$LieuPermis = $unFormulaire->concactComposants($labelLieuPermis, $inputLieuPermis);





$labelJourPermis = $unFormulaire->creerLabel("Date d'obtention du permis sous le format Jour Mois Ann&#233;e :");
$inputJourPermis = $unFormulaire->creerInputTexte('Jour', 'Jour', '' ,'required','ex : 4 et non 04' , '[0-9]+');

$JourPermis = $unFormulaire->concactComposants($labelJourPermis,$inputJourPermis);

$inputMoisPermis = $unFormulaire->creerInputTexte('Mois', 'Mois', '' ,'required','ex : 3 et non 03' , '[0-9]+');

$inputAnneePermis = $unFormulaire->creerInputTexte('Annee', 'Annee', '' ,'required','ex : 1995 et non 95' , '[0-9]+');


$labelPaiementH2 = $unFormulaire->creerLabelH2('Information sur votre moyen de paiement :');


$labelPaiement = $unFormulaire->creerLabel('Mode de paiement :');
$tabPaiement = array("Carte bleu", "liquide","Paypal");
$SelectPaiement = $unFormulaire->creerSelect('ListeDeroulantePaiement','SelectPaiement','Paiement :',$tabPaiement);

$Paiement = $unFormulaire->concactComposants($labelPaiement, $SelectPaiement);

$labelTitulaire = $unFormulaire->creerLabel('Titulaire :');
$inputTitulaire= $unFormulaire->creerInputTexte('Titulaire', 'Titulaire', '' ,'required','ex : Nicolas Pichon' , '[a-zA-Z0-9]+');

$Titulaire = $unFormulaire->concactComposants($labelTitulaire, $inputTitulaire);

$labelCompteCle = $unFormulaire->creerLabel('Compte + cl&#233;');
$inputCompteCle= $unFormulaire->creerInputTexte('CompteCle', 'CompteCle', '' ,'required',' ' , '[a-zA-Z0-9]+');

$CompteCle = $unFormulaire->concactComposants($labelCompteCle, $inputCompteCle);

$labelBanque = $unFormulaire->creerLabel('Nom de votre banque :');
$inputBanque= $unFormulaire->creerInputTexte('NomBanque', 'NomBanque', '' ,'required','ex : BNP ' , '[a-zA-Z0-9]+');

$NomBanque = $unFormulaire->concactComposants($labelBanque, $inputBanque);


$labelBanqueGuichet = $unFormulaire->creerLabel('Votre banque + guichet :');
$inputBanqueGuichet= $unFormulaire->creerInputTexte('BanqueGuichet', 'BanqueGuichet', '' ,'required','ex : Bnp bordeaux Agence 14 rue de la nation ' , '[a-zA-Z0-9]+');

$BanqueGuichet = $unFormulaire->concactComposants($labelBanqueGuichet, $inputBanqueGuichet);


$labelIBAN = $unFormulaire->creerLabel('IBAN :');
$inputIBAN = $unFormulaire->creerInputTexte('IBAN', 'IBAN', '' ,'required','' , '[0-9]+');

$IBAN = $unFormulaire->concactComposants($labelIBAN, $inputIBAN);

$labelBIC = $unFormulaire->creerLabel('BIC :');
$inputBIC = $unFormulaire->creerInputTexte('BIC', 'BIC', '' ,'required','' , '[0-9]+');

$BIC = $unFormulaire->concactComposants($labelBIC, $inputBIC);


$labelLoginH2 = $unFormulaire->creerLabelH2('Identifiant et mot de passe :');

$labelCheckBox = $unFormulaire->creerLabel('Choississez votre mode de Facturation :');

$InputCheckBox = $unFormulaire->creerCheckbox("Facturation","Facturation","E-mail","E-mail");

$CheckBox = $unFormulaire->concactComposants($labelCheckBox,$InputCheckBox);

$CheckBox2 = $unFormulaire->creerCheckbox("Facturation","Facturation","Courrier","courrier");


$unFormulaire->ajouterComposantLigne($Civilite);
$unFormulaire->ajouterComposantLigne($Prenom);
$unFormulaire->ajouterComposantLigne($Nom);
$unFormulaire->ajouterComposantLigne($Sexe);
$unFormulaire->ajouterComposantLigne($Rue);
$unFormulaire->ajouterComposantLigne($Ville);
$unFormulaire->ajouterComposantLigne($CP);
$unFormulaire->ajouterComposantLigne($Fixe);
$unFormulaire->ajouterComposantLigne($Mobile);
$unFormulaire->ajouterComposantLigne($Jour);
$unFormulaire->ajouterComposantLigne($inputMois);
$unFormulaire->ajouterComposantLigne($inputAnnee);

$unFormulaire->ajouterComposantLigne($labelPermisH2);
$unFormulaire->ajouterComposantLigne($NP);
$unFormulaire->ajouterComposantLigne($LieuPermis);
$unFormulaire->ajouterComposantLigne($JourPermis);
$unFormulaire->ajouterComposantLigne($inputMoisPermis);
$unFormulaire->ajouterComposantLigne($inputAnneePermis);

$unFormulaire->ajouterComposantLigne($labelPaiementH2);
$unFormulaire->ajouterComposantLigne($Paiement);
$unFormulaire->ajouterComposantLigne($Titulaire);
$unFormulaire->ajouterComposantLigne($CompteCle);
$unFormulaire->ajouterComposantLigne($NomBanque);
$unFormulaire->ajouterComposantLigne($BanqueGuichet);
$unFormulaire->ajouterComposantLigne($IBAN);
$unFormulaire->ajouterComposantLigne($BIC);
$unFormulaire->ajouterComposantLigne($CheckBox);
$unFormulaire->ajouterComposantLigne($CheckBox2);

$unFormulaire->ajouterComposantLigne($labelLoginH2);





$unFormulaire->ajouterComposantTab();



$unFormulaire->creerFormulaire();







require_once 'vue/vueAbonnement.php' ;

