<?php

if($_SESSION['testauthentification'] != 1){
    $unFormulaire = new Formulaire('post','index.php','connexion','formConnex');
    
    $labelLogin = $unFormulaire->creerLabel('Identifiant :');
    $inputLogin = $unFormulaire->creerInputTexte('login', 'inpLogin', '' ,'required','ex : alexandre...' , '[a-zA-Z0-9]+');
    
    $labelMdp = $unFormulaire->creerLabel('Mot de passe :');
    $inputMdp = $unFormulaire->creerInputMdp('mdp', 'inpMdp','required','ex : leguillou...' , '[a-zA-Z0-9]+');
    
    $identifiant = $unFormulaire->concactComposants($labelLogin, $inputLogin);
    $motpasse = $unFormulaire->concactComposants($labelMdp, $inputMdp);
    
    $submit = $unFormulaire->creerInputSubmit('btnConnex', 'connexion', 'Connecter');
    
    $unFormulaire->ajouterComposantLigne($identifiant);
    $unFormulaire->ajouterComposantLigne($motpasse);
    $unFormulaire->ajouterComposantLigne($submit);
    $unFormulaire->ajouterComposantTab();
    
    
    $unFormulaire->creerFormulaire();
}


require_once 'vue/vueConnexion.php' ;