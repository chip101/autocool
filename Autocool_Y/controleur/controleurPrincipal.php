    <?php

require_once 'fonctions/menu.php';
require_once 'fonctions/formulaire.php';
require_once 'fonctions/dispatcher.php';
require_once 'modele/accesDonnees.php';


$connex = connection($dsn, $user, $pass);

// GET + DANS L URL DE LA PAGE , PERMET DE SAVOIR SUR QUEL MENU ON EST//
if(isset($_GET['AutoMP'])){
	$_SESSION['AutoMP']= $_GET['AutoMP'];
}
else
{
	if(!isset($_SESSION['AutoMP'])){
		$_SESSION['AutoMP']="accueil";
	}
}

//connexion , si la variable de formulaire existe (post_login)on affecte des variables de session et on appelle la fct authentification


if(isset($_POST['login'])){
    $_SESSION['login'] = $_POST['login'];
    $_SESSION['mdp'] = $_POST['mdp'];
    $_SESSION['testauthentification'] = authentification($connex, $_POST['login'], $_POST['mdp']);
}
// Si la variable de session testauthentification n existe pas on la met à 0
else{
    if(!isset($_SESSION['testauthentification'])){
        $_SESSION['testauthentification'] = 0;
    }
}

// Si la variable de session indique qu on est authentifie 
// 

if($_SESSION['testauthentification'] == 1){
    if(isset($_POST['login'])){
        $_SESSION['AutoMP'] = "accueil";
        $idFonction = recupId($connex, $_POST['login'], $_POST['mdp']);
    }
}
// 
else{
    if(isset ($_POST['login'])){
        $message = "login ou mot de passe incorrect";
    }
    else{
        $message ="";
    }
}


// deconnexion

if($_SESSION['testauthentification'] == 1 &&  $_SESSION['AutoMP'] == 'connexion'){
    $_SESSION['testauthentification'] = 0;
    $_SESSION['fonction']=0;
    $_SESSION['AutoMP']="accueil";
}

// ajout de la barre menu avec accueil et connexion si la variable de session testauth est à 0.

$autocool= new Menu("AutoMP");

$autocool->ajouterComposant($autocool->creerItemLien("accueil", "Accueil"));
$autocool->ajouterComposant($autocool->creerItemLien("Abonnement", "Abonnement"));
if($_SESSION['testauthentification'] != 1){
    $autocool->ajouterComposant($autocool->creerItemLien("connexion", "Se connecter"));
}

// Si l'utilisateur est connecté on crée le menu deconnexion 
else{
   
    $autocool->ajouterComposant($autocool->creerItemLien("connexion", "Deconnexion"));
}

$menuPrincipal = $autocool->creerMenu($_SESSION['AutoMP'],'AutoMP');



include_once dispatcher::dispatch($_SESSION['AutoMP']);

