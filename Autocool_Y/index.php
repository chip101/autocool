<?php require 'fonctions/monLoader.php';
 session_start()?>

<!DOCTYPE html>
<html lang="fr">
	<head>
	
		<meta charset="utf-8" />
		<title>Autocool</title>
		<link rel="shortcut icon" type="image/x-icon" href="images/shortcut.ico" />
		
		<style type="text/css">
			@import url(styles/autocool.css);
		</style>
	
	</head>
	<body >
		<?php
			require_once 'controleur/controleurPrincipal.php';
		?>
	</body>
</html>